package com.aody.aodyapp.service;

import com.aody.aodyapp.entity.LigneSubscription;
import com.aody.aodyapp.entity.Subscription;
import com.aody.aodyapp.repository.LigneSubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class LigneSubscriptionService implements IService<LigneSubscription> {
    @Autowired
    private LigneSubscriptionRepository ligneSubscriptionRepository;
    @Override
    public List<LigneSubscription> findAll() {
        return ligneSubscriptionRepository.findAll();
    }

    @Override
    public LigneSubscription findById(Integer id) {
        return ligneSubscriptionRepository.findById(id).get();
    }

    @Override
    public String save(LigneSubscription obj) {
        ligneSubscriptionRepository.save(obj);
        return null;
    }

    @Override
    public String deleteById(Integer id) {
        ligneSubscriptionRepository.deleteById(id);
        return null;
    }
    public List<LigneSubscription> findBySubscription(Subscription subscription){
        return ligneSubscriptionRepository.findBySubscription(subscription);
    }

    public LigneSubscription updateLigneSubscription(LigneSubscription newligneSubscription){
        LigneSubscription oldligneSubscription=ligneSubscriptionRepository.findBySeriesNumber(newligneSubscription.getSeriesNumber());
        oldligneSubscription.setSubscription(newligneSubscription.getSubscription());
        return ligneSubscriptionRepository.save(oldligneSubscription);
    }
}
