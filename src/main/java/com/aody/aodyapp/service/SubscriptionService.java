package com.aody.aodyapp.service;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.Forfait;
import com.aody.aodyapp.entity.Subscription;
import com.aody.aodyapp.repository.SubscriptionRepository;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SubscriptionService {
    @Autowired
    private SubscriptionRepository subscriptionRepository;

    public List<Subscription> findAll() {
        return subscriptionRepository.findAll();
    }


    public Subscription findById(Integer id) {
        return subscriptionRepository.findById(id).get();
    }


    public Subscription save(Subscription obj) {
        return subscriptionRepository.save(obj);
    }


    public String deleteById(Integer id) {
        subscriptionRepository.deleteById(id);
        return null;
    }
    public List<Subscription> getSubscriptionsByCompany(Company company) {
        return subscriptionRepository.findByCompany(company);
    }
    public Subscription validerPaiement(Subscription subscription,Company company){
        Stripe.apiKey="sk_test_51OVU2MIpGXDhtkOypglN3HWzibDAhzD00YP0uRlDqBBsT58uXNxBqrVFAFoXRtBfi21faT2r4Nh0LdSf5jjkFD0x00z2mWN97j";
        Map<String,Object> params= new HashMap<>();
        params.put("amount", subscription.getPrice().multiply(new BigDecimal(100)).intValue());
        params.put("currency","MGA");
        params.put("description", subscription.getDescription());
        params.put("source","tok_visa");

        try {
            Charge charge= Charge.create(params);
            subscription.setStatus(charge.getStatus());
            subscription.setIdStripe(charge.getId());
            subscription.setCompany(company);
            return subscriptionRepository.save(subscription);
        } catch (StripeException e) {
            System.out.println("stripe error"+e.getMessage());
            return subscription;
        }
    }
}
