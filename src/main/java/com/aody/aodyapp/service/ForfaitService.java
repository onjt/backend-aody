package com.aody.aodyapp.service;


import com.aody.aodyapp.repository.ForfaitRepository;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import com.aody.aodyapp.entity.Forfait;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional
public class ForfaitService implements IService<Forfait>{
    @Autowired
    private ForfaitRepository forfaitRepository;

    @Override
    public List<Forfait> findAll() {
        return forfaitRepository.findAll();
    }

    @Override
    public Forfait findById(Integer id) {
        Optional<Forfait> optionalForfait = forfaitRepository.findById(id);
        return optionalForfait.orElseThrow(() -> new NoSuchElementException("Forfait non trouvé pour l'ID : " + id));
    }

    @Override
    public String save(Forfait obj) {
        forfaitRepository.save(obj);
        return null;
    }

    @Override
    public String deleteById(Integer id) {
        forfaitRepository.deleteById(id);
        return null;
    }


}
