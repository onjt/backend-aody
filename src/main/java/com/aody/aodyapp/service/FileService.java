package com.aody.aodyapp.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {
    String uploadImage(String path, MultipartFile file,Integer id) throws IOException;
    byte[] downloadImage(Integer id, String filePath) throws IOException;
}
