package com.aody.aodyapp.service;

import java.util.List;

public interface IService <E> {
    public List<E> findAll();
    public E findById(Integer id);
    public String save(E obj);
    public String deleteById(Integer id);
}
