package com.aody.aodyapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aody.aodyapp.entity.JobOffer;
import com.aody.aodyapp.repository.JobOfferRepository;

@Service
@Transactional
public class JobOfferService implements IService<JobOffer> {

    @Autowired
    private JobOfferRepository jobOfferRepository;

    @Override
    public List<JobOffer> findAll() {
        return jobOfferRepository.findAll();
    }

    @Override
    public JobOffer findById(Integer id) {
        Optional<JobOffer> optionalJobOffer = jobOfferRepository.findById(id);
        return optionalJobOffer.orElse(null);
    }

    @Override
    public String save(JobOffer jobOffer) {
        jobOfferRepository.save(jobOffer);
        return "Offre d'emploi enregistrée avec succès.";
    }

    public JobOffer saveJobOffer(JobOffer jobOffer) {
        try {
            return jobOfferRepository.save(jobOffer);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public String deleteById(Integer id) {
        Optional<JobOffer> optionalJobOffer = jobOfferRepository.findById(id);
        if (optionalJobOffer.isPresent()) {
            jobOfferRepository.deleteById(id);
            return "Offre d'emploi supprimée avec succès.";
        } else {
            return "Offre d'emploi introuvable.";
        }
    }

    public List<JobOffer> findByAccountId(Integer idAccount) {
        return jobOfferRepository.findByCompanyId(idAccount);
    }

    public List<Object[]> findJobOffersWithCompanyByStatusOffer(String statusOffer) {
        return jobOfferRepository.findJobOffersWithCompanyByStatusOffer(statusOffer);
    }
}
