package com.aody.aodyapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.Student;
import com.aody.aodyapp.repository.CompanyRepository;
import com.aody.aodyapp.repository.StudentRepository;

@Service
public class AuthService {
    @Autowired
    private CompanyRepository companyRepository;
    
    @Autowired
    private StudentRepository studentRepository;

    public boolean authenticateCompany(String email, String password) {
        Company company = companyRepository.findByEmail(email);
        if (company != null) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return passwordEncoder.matches(password, company.getPassword());
        }
        return false;
    }

    public boolean authenticateStudent(String email, String password) {
        Student student = studentRepository.findByEmail(email);
        if (student != null) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            return passwordEncoder.matches(password, student.getPassword());
        }
        return false;
    }
}