package com.aody.aodyapp.service.impl;

import com.aody.aodyapp.service.FileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {
    @Override
    public String uploadImage(String path, MultipartFile file, Integer id) throws IOException {

        String name = file.getOriginalFilename();
        String randomID = UUID.randomUUID().toString();
        String fileName = randomID.concat(name.substring(name.lastIndexOf(".")));
        String filePath = path + File.separator+fileName;
        File f = new File(path);

        if (!f.exists()){
            f.mkdir();
        }

        Files.copy(file.getInputStream(), Paths.get(filePath));
        return path+fileName;
    }
    @Override
    public byte[] downloadImage(Integer id, String filePath) throws IOException {
        byte[] image = Files.readAllBytes(new File(filePath).toPath());
        return image;
    }
}
