package com.aody.aodyapp.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.service.impl.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aody.aodyapp.entity.Student;
import com.aody.aodyapp.repository.StudentRepository;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class StudentService implements IService<Student> {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private FileServiceImpl fileService;

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public String save(Student obj) {

        studentRepository.save(obj);

        return null;
    }

    @Override
    public String deleteById(Integer id) {

        studentRepository.deleteById(id);

        return null;
    }

    public List<Student> findByStatusAccount(String statusAccount) {
        return studentRepository.findByStatusAccount(statusAccount);
    }

    public Student findByEmail(String email) {
        return studentRepository.findByEmail(email);
    }

    // todo : modification sur les autres colonnes
    public Student updateStudent(Student updatedStudent) {
        Student existingStudent = studentRepository.findById(updatedStudent.getIdAccount())
                .orElseThrow(() -> new IllegalArgumentException("Étudiant introuvable"));

        existingStudent.setStatusAccount(updatedStudent.getStatusAccount());

        return studentRepository.save(existingStudent);
    }

    public Student updatePhoto(String path, MultipartFile file, Integer id) throws IOException{
        Student existingStudent = studentRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("Student introuvable"));

        String pathPhoto = fileService.uploadImage(path,file,existingStudent.getIdAccount());
        existingStudent.setPhoto(pathPhoto);

        return studentRepository.save(existingStudent);
    }

    public byte[] downloadPhoto(Student student) throws IOException {

        String filePath = student.getPhoto();
        return Files.readAllBytes(new File(filePath).toPath());
    }

}
