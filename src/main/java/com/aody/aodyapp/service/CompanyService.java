package com.aody.aodyapp.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.aody.aodyapp.service.impl.FileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.repository.CompanyRepository;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class CompanyService implements IService<Company>{

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private FileServiceImpl fileService;

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public Company findById(Integer id) {
        return companyRepository.findById(id).get();
    }

    @Override
    public String save(Company company) {
        companyRepository.save(company);
        return null;
    }

    @Override
    public String deleteById(Integer id) {
        companyRepository.deleteById(id);
        return null;
    }

    public Company findByEmail(String email) {
        return companyRepository.findByEmail(email);
    }

    public Company validate(Company updatedCompany){
        Company existingCompany = companyRepository.findById(updatedCompany.getIdAccount())
                .orElseThrow(() -> new IllegalArgumentException("Company introuvable"));

        existingCompany.setStatusAccount(updatedCompany.getStatusAccount());
        return companyRepository.save(existingCompany);
    }
    public Company update(Company updatedCompany){

        Company existingCompany = companyRepository.findById(updatedCompany.getIdAccount())
                .orElseThrow(() -> new IllegalArgumentException("Company introuvable"));

        existingCompany.setEmail(updatedCompany.getEmail());
        existingCompany.setPhone(updatedCompany.getPhone());
        existingCompany.setName(updatedCompany.getName());
        existingCompany.setWebsite(updatedCompany.getWebsite());
        existingCompany.setSince(updatedCompany.getSince());
        existingCompany.setTeamSize(updatedCompany.getTeamSize());
        existingCompany.setDescriptionCompany(updatedCompany.getDescriptionCompany());



        return companyRepository.save(existingCompany);
    }

    public Company updatePhoto(String path, MultipartFile file, Integer id) throws IOException {

        Company existingCompany = companyRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Company introuvable"));


        String pathPhoto = fileService.uploadImage(path,file,existingCompany.getIdAccount());
        existingCompany.setPhoto(pathPhoto);

        return companyRepository.save(existingCompany);
    }

    public byte[] downloadPhoto(Company company) throws IOException {

        String filePath = company.getPhoto();
        return Files.readAllBytes(new File(filePath).toPath());
    }

}
