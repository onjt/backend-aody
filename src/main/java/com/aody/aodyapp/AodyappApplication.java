package com.aody.aodyapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.aody.aodyapp")
public class AodyappApplication {

	public static void main(String[] args) {
		System.out.println("lancement du spring");
		SpringApplication.run(AodyappApplication.class, args);
	}
}
