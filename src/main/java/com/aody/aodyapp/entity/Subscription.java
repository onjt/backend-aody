package com.aody.aodyapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "subscription")
public class Subscription implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idSubscription")
    private Integer idSubscription;

    @Column(nullable = true)
	private Date subscriptionDate;

    @Column
    private String typePaiement;

    @Column
    private String idStripe;

    @Column
    private String status;

    @Column
    private BigDecimal price;

    @Column
    private String description;

    @JsonIgnore
    @ManyToOne
    public Company company;

    @JsonIgnore
    @OneToMany(mappedBy = "subscription")
    public List<LigneSubscription> ligneSubscription = new ArrayList<LigneSubscription>();
}
