package com.aody.aodyapp.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="student")
public class Student extends Account{
    @Column
    private String name;
    @Column
    private String qualification;
    @Column
    private String language;
    @Column
    private String jobCategory;
    @Column
    private String experience;
    @Column
    private double currentSalary;
    @Column
    private double expectedSalary;
    @Column
    private String dateOfBirth;
    @Column
    private String country;
    @Column
    private String city;
    @Column
    private int postcode;
    @Column
    private String fullAddress;
    @Column(nullable = true, length = 700)
    private String descriptionStudent;
    @Column
    private String gender;


}