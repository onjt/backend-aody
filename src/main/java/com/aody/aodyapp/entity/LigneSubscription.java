package com.aody.aodyapp.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="lignesubscription")
public class LigneSubscription {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "seriesnumber", nullable = false)
    private Integer seriesNumber;
    @Column(nullable = true)
	private Date startDateSubscription;

    @Column(nullable = true)
	private Date endDateSubscription;

    @Column(nullable = true)
	private BigDecimal purchasePrice;

    @Column(nullable = true)
	private String statusLigneSubscription;

    @ManyToOne
    @JsonIgnore
    public Subscription subscription;

    @ManyToOne
    @JsonIgnore
	public Forfait forfait;
}
