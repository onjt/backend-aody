package com.aody.aodyapp.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "forfait")
public class Forfait {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idPackage")
    private Integer idPackage;

	@Column(nullable = true)
	private String packageName;

	@Column(nullable = true)
	private BigDecimal price;

	@Column(nullable = true)
	private int jobPosting;

	@Column(nullable = true)
	private int boostJob;

	@Column(nullable = true)
	private int jobDisplayed;

	@Column(nullable = true)
	private boolean premiumSupport;

	@JsonIgnore
	@OneToMany(mappedBy = "forfait")
	public List<LigneSubscription> ligneSubscription = new ArrayList<LigneSubscription>();
}
