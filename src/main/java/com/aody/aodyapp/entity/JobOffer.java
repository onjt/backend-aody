package com.aody.aodyapp.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="joboffer")
public class JobOffer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idOffer", nullable = false)
    private Integer idOffer;
    @Column(nullable = true)
    private String jobTitle;
    @Column(nullable = true)
    private String jobCategory;
    @Column(nullable = true)
    private String jobType;
    @Column(nullable = true)
    private String jobDescription;
    @Column(nullable = true)
    private Double offeredSalary;
    @Column(nullable = true)
    private String experience;
    @Column(nullable = true)
    private String qualification;
    @Column(nullable = true)
    private String requirements;
    @Column(nullable = true)
    private String responsabilities;
    @Column(nullable = true)
    private String city;
    @Column(nullable = true)
    private String location;
    @Column(nullable = true)
    private String latitude;
    @Column(nullable = true)
    private String country;
    @Column(nullable = true)
    private String longitude;
    @Column(nullable = true)
    private Date startDateJobOffer;
    @Column(nullable = true)
    private Date endDateJobOffer;
    @Column(nullable = true)
    private Date publicationDate;
    @Column(nullable = true)
    private String statusOffer;
    @ManyToOne
    @JoinColumn(name = "company_id_account", referencedColumnName = "idAccount", nullable = false)
    public Company company;
}
