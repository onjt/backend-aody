package com.aody.aodyapp.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="company")
public class Company extends Account {
    @Column(nullable = true)
    private String name;

    @Column(nullable = true)
    private String website;

    @Column(nullable = true)
    private Date since;

    @Column(nullable = true)
    private String teamSize;

    @Column(nullable = true, length = 700)
    private String descriptionCompany;

    @Column(nullable = true)
    private String headquarter;

    @JsonIgnore
    @OneToMany(mappedBy = "company")
    private List<JobOffer> jobOffers = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy= "company")
    public List<Subscription> subscriptions= new ArrayList<>();
}
