package com.aody.aodyapp.entity;

import java.io.Serializable;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idAccount;

    @Column(unique = true)
    private String email;
    @Column
    private String password;
    @Column
    private String photo;
    @Column
    private String phone;
    @Column
    private String statusAccount;
    @Column(nullable = false, updatable = false)
    private String creationDate;
}