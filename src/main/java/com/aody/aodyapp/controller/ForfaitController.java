package com.aody.aodyapp.controller;

import com.aody.aodyapp.entity.Forfait;
import com.aody.aodyapp.service.ForfaitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/forfait")
@CrossOrigin("http://localhost:3000")
public class ForfaitController implements IController<Forfait> {
    @Autowired
    private ForfaitService forfaitservice;
    @Override
    public List<Forfait> findAll() {
        return forfaitservice.findAll();
    }

    @Override
    public Forfait findById(Integer id) {
        return forfaitservice.findById(id);
    }

    @Override
    public Forfait save(Forfait obj) {
        forfaitservice.save(obj);
        return obj;
    }

    @Override
    public String deleteById(Long id) {
        return null;
    }


}
