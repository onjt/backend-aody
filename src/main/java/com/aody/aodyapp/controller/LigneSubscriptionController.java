package com.aody.aodyapp.controller;

import com.aody.aodyapp.entity.LigneSubscription;
import com.aody.aodyapp.entity.Forfait;
import com.aody.aodyapp.entity.Subscription;
import com.aody.aodyapp.service.LigneSubscriptionService;
import com.aody.aodyapp.service.ForfaitService;
import com.aody.aodyapp.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/ligneSubscription")
@CrossOrigin("http://localhost:3000")
public class LigneSubscriptionController implements IController<LigneSubscription> {
    @Autowired
    private LigneSubscriptionService ligneSubscriptionService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private ForfaitService forfaitservice;
    @Override
    public List<LigneSubscription> findAll() {
        return ligneSubscriptionService.findAll();
    }

    @Override
    public LigneSubscription findById(Integer id) {
        return ligneSubscriptionService.findById(id);

    }

    @Override
    public LigneSubscription save(LigneSubscription obj) {
        ligneSubscriptionService.save(obj);
        return obj;
    }

    @Override
    public String deleteById(Long id) {
        return null;
    }

    @PostMapping("/createLigne/{idForfait}/{idSubscription}")
    public ResponseEntity<String> createLigne(@PathVariable Integer idForfait, @PathVariable Integer idSubscription, @RequestBody LigneSubscription ligneSubscription) {
        Forfait forfait = forfaitservice.findById(idForfait);
        Subscription subscription = subscriptionService.findById(idSubscription);

        if (forfait == null || subscription == null) {
            // Gestion de l'erreur si Forfait ou Subscription n'est pas trouvé
            return new ResponseEntity<>("Forfait ou Subscription non trouvé", HttpStatus.NOT_FOUND);

        }
        ligneSubscription.setPurchasePrice(forfait.getPrice());
        ligneSubscription.setSubscription(subscription);
        ligneSubscription.setForfait(forfait);
        ligneSubscriptionService.save(ligneSubscription);

        return new ResponseEntity<>("LigneSubscription créée avec succès", HttpStatus.CREATED);
    }
    @GetMapping("/listPerSubscription/{idSubscription}")
    public ResponseEntity<List<LigneSubscription>> listPerSubscription(@PathVariable Integer idSubscription) {
        try {
            Subscription subscription = subscriptionService.findById(idSubscription);
            List<LigneSubscription> ligneSubscriptions = ligneSubscriptionService.findBySubscription(subscription);
            return new ResponseEntity<>(ligneSubscriptions, HttpStatus.OK);
        } catch (Exception e) {
            // Log the exception for debugging purposes
            ResponseEntity.badRequest().body("Bad request");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
