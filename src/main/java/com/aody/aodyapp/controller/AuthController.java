package com.aody.aodyapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.Student;
import com.aody.aodyapp.service.AuthService;
import com.aody.aodyapp.service.CompanyService;
import com.aody.aodyapp.service.StudentService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/login")
public class AuthController {
    @Autowired
    private AuthService authService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private StudentService studentService;

    @PostMapping("/loginCompany")
    public ResponseEntity<Object> loginCompany(@RequestBody Company company) {
        String email = company.getEmail();
        String password = company.getPassword();

        Company storedCompany = companyService.findByEmail(email);

        if (storedCompany == null) {
            return ResponseEntity.notFound().build();

        }
        if (!storedCompany.getStatusAccount().equals("Activé")) {
            return ResponseEntity.badRequest().body("Votre Email ou mot de passe est invalide, veuillez vérifier ou créer un compte");
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (passwordEncoder.matches(password, storedCompany.getPassword())) {
            return ResponseEntity.ok(storedCompany);
        } else {
            return ResponseEntity.badRequest().body("Votre Email ou mot de passe est invalide, veuillez vérifier ou créer un compte");
        }
    }

    @PostMapping("/loginStudent")
    public ResponseEntity<Object> loginStudent(@RequestBody Student student) {
        String email = student.getEmail();
        String password = student.getPassword();

        Student storedStudent = studentService.findByEmail(email);

        if (storedStudent == null) {
            return ResponseEntity.notFound().build();
        }

        if (!storedStudent.getStatusAccount().equals("Activé")) {
            return ResponseEntity.badRequest().body("Votre Email ou mot de passe est invalide, veuillez vérifier ou créer un compte");
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (passwordEncoder.matches(password, storedStudent.getPassword())) {
            return ResponseEntity.ok(storedStudent);
        } else {
            return ResponseEntity.badRequest().body("Votre Email ou mot de passe est invalide, veuillez vérifier ou créer un compte");
        }
    }
}