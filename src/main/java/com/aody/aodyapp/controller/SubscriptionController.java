package com.aody.aodyapp.controller;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.LigneSubscription;
import com.aody.aodyapp.entity.Subscription;
import com.aody.aodyapp.repository.LigneSubscriptionRepository;
import com.aody.aodyapp.service.CompanyService;
import com.aody.aodyapp.service.LigneSubscriptionService;
import com.aody.aodyapp.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
@RestController
@RequestMapping("/subscription")
@CrossOrigin("http://localhost:3000")
public class SubscriptionController implements IController<Subscription> {
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private LigneSubscriptionService ligneSubscriptionService;
    @Override
    public List<Subscription> findAll() {
        return subscriptionService.findAll();
    }

    @Override
    public Subscription findById(Integer id) {
        return subscriptionService.findById(id);
    }

    @Override
    public Subscription save(Subscription obj) {
        return subscriptionService.save(obj);
    }

    @Override
    public String deleteById(Long id) {
        return null;
    }

    @GetMapping("/company/{companyId}")
    public List<Subscription> getSubscriptionsByCompany(@PathVariable Integer companyId) {
        Company company = companyService.findById(companyId);
        // Obtenez la liste des abonnements par entreprise
        return subscriptionService.getSubscriptionsByCompany(company);
    }
    @PostMapping("/subscribe/{subscriptionId}/{companyId}")
    public Subscription validerSubscription(@PathVariable Integer companyId,@PathVariable Integer subscriptionId,@RequestBody Subscription subscription){
        Company company = companyService.findById(companyId);
        subscription.setIdSubscription(subscriptionId);
        Subscription existingSubscription=subscriptionService.findById(subscription.getIdSubscription());
        List<LigneSubscription> ligneSubscriptions=ligneSubscriptionService.findBySubscription(existingSubscription);
        BigDecimal total = BigDecimal.ZERO;
        for (LigneSubscription ligneSubscription:ligneSubscriptions){
            total= ligneSubscription.getPurchasePrice().add(total);
        }
        existingSubscription.setPrice(total);
        return subscriptionService.validerPaiement(existingSubscription,company);
    }

}
