package com.aody.aodyapp.controller;


import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface IController<E> {
    @GetMapping("/list")
    public List<E> findAll();
    @GetMapping("/list/{id}")
    public E findById(@PathVariable("id") Integer id);
    @PostMapping("/save")
    public E save(@RequestBody E obj);
    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable("id") Long id);
}
