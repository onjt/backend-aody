package com.aody.aodyapp.controller;

import java.io.IOException;
import java.util.List;

import com.aody.aodyapp.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.aody.aodyapp.entity.Student;
import com.aody.aodyapp.service.StudentService;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/student")
@CrossOrigin("http://localhost:3000")
public class StudentController implements IController<Student>{
    @Value("${project.image}")
    private String path;
    @Autowired
    private StudentService studentService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Student> findAll() {
        return studentService.findAll();
    }

    public Student findById(Integer id) {
        return null;
    }

    public Student save(Student obj) {
        String hashedPassword = passwordEncoder.encode(obj.getPassword());
        obj.setPassword(hashedPassword);
        studentService.save(obj);
        return obj;
    }

    public String deleteById(Long id) {
        return null;
    }

    @PutMapping("/updatestate/{id}")
    public String validerCompte(@PathVariable Integer id, @RequestBody Student updateState) {
        Student compte = studentService.findById(id);
        if (compte != null) {
            compte.setStatusAccount(updateState.getStatusAccount());
            studentService.save(compte);
            return "Compte validé avec succès";
        } else {
            return "Compte non trouvé";
        }
    }

    @GetMapping("/listvalide/{state}")
    public List<Student> findByStateStudent(@PathVariable("state") String statusAccount) {
        return studentService.findByStatusAccount(statusAccount);
    }

    @PutMapping("/update/{id}")
    public void updateStudent(@PathVariable("id") Integer id, @RequestBody Student updatedStudent){
        updatedStudent.setIdAccount(id);
        Student savedStudent = studentService.updateStudent(updatedStudent);
    }

    @PostMapping("/search")
    public ResponseEntity<?> findByEmail(@RequestBody String email) {
        Student student = studentService.findByEmail(email);
        if (student != null) {
            return ResponseEntity.ok(student);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/update-photo/{id}")
    public void updatePhoto(
            @RequestParam("image") MultipartFile image,
            @PathVariable("id") Integer id
    ) throws IOException {

        studentService.updatePhoto(path,image,id);
    }

    @GetMapping("/download-photo/{id}")
    public ResponseEntity<?> downloadPhoto(@PathVariable("id") Integer id) throws IOException {

        Student existingStudent = studentService.findById(id);
        byte[] imageData = studentService.downloadPhoto(existingStudent);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }

}
