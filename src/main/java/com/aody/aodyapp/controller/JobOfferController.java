package com.aody.aodyapp.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.JobOffer;
import com.aody.aodyapp.repository.CompanyRepository;
import com.aody.aodyapp.service.JobOfferService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/joboffers")
public class JobOfferController implements IController<JobOffer> {

    @Autowired
    private JobOfferService jobOfferService;

    @Autowired
    private CompanyRepository companyRepository;

    private static final String LIST_ENDPOINT = "/lists";
    private static final String GET_BY_ID_ENDPOINT = "/{id}";
    private static final String SAVE_ENDPOINT = "/saves";
    private static final String DELETE_ENDPOINT = "/deletes/{id}";
    private static final String UPDATE_ENDPOINT = "/updates/{id}";
    private static final String LIST_BY_ACCOUNT_ENDPOINT = "/listByAccount/{idAccount}";

    @GetMapping(LIST_ENDPOINT)
    public ResponseEntity<List<JobOffer>> getAllJobOffers() {
        try {
            List<JobOffer> jobOffers = jobOfferService.findAll();
            return ResponseEntity.ok().body(jobOffers);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(GET_BY_ID_ENDPOINT)
    public ResponseEntity<JobOffer> getJobOfferById(@PathVariable Integer id) {
        try {
            JobOffer jobOffer = jobOfferService.findById(id);
            return (jobOffer != null) ? ResponseEntity.ok().body(jobOffer) : ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(SAVE_ENDPOINT)
    public ResponseEntity<JobOffer> saveJobOffer(@RequestBody JobOffer jobOffer) {
        try {
            Company existingCompany = companyRepository.findById(jobOffer.getCompany().getIdAccount())
                    .orElse(null);

            if (existingCompany == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            jobOffer.setCompany(existingCompany);

            JobOffer savedJobOffer = jobOfferService.saveJobOffer(jobOffer);

            return new ResponseEntity<>(savedJobOffer, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(DELETE_ENDPOINT)
    public ResponseEntity<String> deleteJobOfferById(@PathVariable Integer id) {
        String responseMessage = jobOfferService.deleteById(id);
        return ResponseEntity.ok(responseMessage);
    }

    @PutMapping(UPDATE_ENDPOINT)
    public ResponseEntity<String> updateJobOffer(@PathVariable Integer id, @RequestBody JobOffer updatedJobOffer) {
        JobOffer existingJobOffer = jobOfferService.findById(id);

        if (existingJobOffer != null) {
            existingJobOffer.setJobTitle(updatedJobOffer.getJobTitle());
            existingJobOffer.setJobCategory(updatedJobOffer.getJobCategory());
            existingJobOffer.setJobType(updatedJobOffer.getJobType());
            existingJobOffer.setJobDescription(updatedJobOffer.getJobDescription());
            existingJobOffer.setOfferedSalary(updatedJobOffer.getOfferedSalary());
            existingJobOffer.setExperience(updatedJobOffer.getExperience());
            existingJobOffer.setQualification(updatedJobOffer.getQualification());
            existingJobOffer.setRequirements(updatedJobOffer.getRequirements());
            existingJobOffer.setResponsabilities(updatedJobOffer.getResponsabilities());
            existingJobOffer.setCity(updatedJobOffer.getCity());
            existingJobOffer.setLocation(updatedJobOffer.getLocation());
            existingJobOffer.setLatitude(updatedJobOffer.getLatitude());
            existingJobOffer.setCountry(updatedJobOffer.getCountry());
            existingJobOffer.setLongitude(updatedJobOffer.getLongitude());
            existingJobOffer.setStartDateJobOffer(updatedJobOffer.getStartDateJobOffer());
            existingJobOffer.setEndDateJobOffer(updatedJobOffer.getEndDateJobOffer());
            existingJobOffer.setPublicationDate(updatedJobOffer.getPublicationDate());
            existingJobOffer.setStatusOffer(updatedJobOffer.getStatusOffer());
            jobOfferService.save(existingJobOffer);

            return ResponseEntity.ok("Job offer mise à jour avec succès");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Job offer non trouvé");
        }
    }

    @GetMapping(LIST_BY_ACCOUNT_ENDPOINT)
    public ResponseEntity<List<JobOffer>> getJobOffersByAccountId(@PathVariable Integer idAccount) {
        try {
            List<JobOffer> jobOffers = jobOfferService.findByAccountId(idAccount);

            // Filtrena izay JobOffer manana statusOffer = supprimé dia tsy alefa izany izay manana io status io
            List<JobOffer> filteredJobOffers = jobOffers.stream()
                    .filter(jobOffer -> !"Supprimé".equals(jobOffer.getStatusOffer()))
                    .collect(Collectors.toList());

            return ResponseEntity.ok().body(filteredJobOffers);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/updateStatus/{id}")
    public ResponseEntity<String> updateJobOfferStatus(@PathVariable Integer id) {
        try {
            JobOffer existingJobOffer = jobOfferService.findById(id);

            if (existingJobOffer != null) {
                existingJobOffer.setStatusOffer("Supprimé");
                jobOfferService.save(existingJobOffer);

                return ResponseEntity.ok("Le statut de l'offre d'emploi a été mis à jour avec succès.");
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Offre d'emploi non trouvée");
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Erreur lors de la mise à jour du statut de l'offre d'emploi", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//Ho ampiasaiko @ recherche multicritère ito
    private static final String LIST_POSTED_ENDPOINT = "/listPosted";

    @GetMapping(LIST_POSTED_ENDPOINT)
    public ResponseEntity<List<Object[]>> getPostedJobOffersWithCompany() {
        try {
            List<Object[]> postedJobOffersWithCompany = jobOfferService.findJobOffersWithCompanyByStatusOffer("posté");
            return ResponseEntity.ok().body(postedJobOffersWithCompany);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/updateStatusCloture/{id}")
    public ResponseEntity<String> updateJobOfferCloture(@PathVariable Integer id, @RequestBody JobOffer updatedJobOffer) {
        JobOffer existingJobOffer = jobOfferService.findById(id);

        if (existingJobOffer != null) {
            existingJobOffer.setStatusOffer(updatedJobOffer.getStatusOffer());
            jobOfferService.save(existingJobOffer);

            return ResponseEntity.ok("Job offer mise à jour avec succès");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Job offer non trouvé");
        }
    }


    @Override
    public List<JobOffer> findAll() {
        return null;
    }

    @Override
    public JobOffer findById(Integer id) {
        return null;
    }

    @Override
    public JobOffer save(JobOffer obj) {
        return null;
    }

    @Override
    public String deleteById(Long id) {
        return null;
    }
}
