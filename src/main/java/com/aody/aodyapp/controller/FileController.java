package com.aody.aodyapp.controller;

import com.aody.aodyapp.playload.FileResponse;
import com.aody.aodyapp.service.CompanyService;
import com.aody.aodyapp.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/file")
@CrossOrigin("http://localhost:3000")
public class FileController {
    @Autowired
    private FileService fileService;
    @Autowired
    private CompanyService companyService;
    @Value("${project.image}")
    private String path;
    @PutMapping("/upload/{id}")
    public ResponseEntity<FileResponse> fileUpload(
            @RequestParam("image") MultipartFile image,
            @PathVariable("id") Integer id
            ){
        String fileName = null;

        try {
            fileName = this.fileService.uploadImage(path, image,id);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new FileResponse(fileName,"Not found"),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(new FileResponse(fileName,"Success"),HttpStatus.OK);
    }


    @GetMapping("/download/{id}")
    public ResponseEntity<?> fileDownload(@PathVariable("id") Integer id) throws IOException {
        byte[] imageData = this.fileService.downloadImage(id,"");

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }
}
