package com.aody.aodyapp.controller;

import java.io.IOException;
import java.util.List;

import com.aody.aodyapp.service.LigneSubscriptionService;
import com.aody.aodyapp.service.ForfaitService;
import com.aody.aodyapp.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.service.CompanyService;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/company")
@CrossOrigin("http://localhost:3000")
public class CompanyController implements IController<Company>{

    @Value("${project.image}")
    private String path;
    @Autowired
    private CompanyService companyService;

    @Autowired
    private ForfaitService forfaitservice;
    @Autowired
    private LigneSubscriptionService ligneSubscriptionService;
    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List <Company>findAll() {
        return companyService.findAll();
    }

    @Override
    public Company findById(Integer id) {
        return companyService.findById(id);
    }

    @Override
    public Company save(Company company) {
        String hashedPassword = passwordEncoder.encode(company.getPassword());
        company.setPassword(hashedPassword);
        companyService.save(company);
        return null;
    }
    @Override
    public String deleteById(Long id) {
        return null;
    }

    @PutMapping("/validate/{id}")
    public void validateCompany(@PathVariable("id") Integer id, @RequestBody Company updatedCompany){
        updatedCompany.setIdAccount(id);
        companyService.validate(updatedCompany);
    }

    @PutMapping("/update/{id}")
    public void updateCompany(@PathVariable("id") Integer id, @RequestBody Company updatedCompany){
        updatedCompany.setIdAccount(id);
        companyService.update(updatedCompany);
    }

    @PutMapping("/update-photo/{id}")
    public void updatePhoto(
            @RequestParam("image") MultipartFile image,
            @PathVariable("id") Integer id
    ) throws IOException {

        companyService.updatePhoto(path,image,id);
    }

    @GetMapping("/download-photo/{id}")
    public ResponseEntity<?> downloadPhoto(@PathVariable("id") Integer id) throws IOException {

        Company existingCompany = companyService.findById(id);
        byte[] imageData = companyService.downloadPhoto(existingCompany);

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }

}
