package com.aody.aodyapp.repository;

import com.aody.aodyapp.entity.Forfait;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForfaitRepository extends JpaRepository<Forfait, Integer> {
}
