package com.aody.aodyapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aody.aodyapp.entity.Student;

public interface StudentRepository extends JpaRepository<Student,Integer> {
    Student findByEmail(String email);
    List<Student> findByStatusAccount(String statusAccount);
}
