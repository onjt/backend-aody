package com.aody.aodyapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.aody.aodyapp.entity.JobOffer;

import java.util.List;

@Repository
public interface JobOfferRepository extends JpaRepository<JobOffer, Integer> {
    @Query("SELECT j FROM JobOffer j WHERE j.company.idAccount = :idAccount")
    List<JobOffer> findByCompanyId(@Param("idAccount") Integer idAccount);

    @Query("SELECT j, c FROM JobOffer j JOIN j.company c WHERE j.statusOffer = :statusOffer")
    List<Object[]> findJobOffersWithCompanyByStatusOffer(@Param("statusOffer") String statusOffer);
}
