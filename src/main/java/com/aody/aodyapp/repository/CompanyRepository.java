package com.aody.aodyapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aody.aodyapp.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer>{
    Company findByEmail(String email);
    List<Company> findByStatusAccount(String statusAccount);
}
