package com.aody.aodyapp.repository;


import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {
    List<Subscription> findByCompany(Company company);
}
