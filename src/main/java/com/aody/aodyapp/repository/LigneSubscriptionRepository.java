package com.aody.aodyapp.repository;

import com.aody.aodyapp.entity.Company;
import com.aody.aodyapp.entity.LigneSubscription;
import com.aody.aodyapp.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LigneSubscriptionRepository extends JpaRepository<LigneSubscription, Integer> {
    public List<LigneSubscription> findBySubscription(Subscription subscription);
    public LigneSubscription findBySeriesNumber(Integer seriesNumber);
}
